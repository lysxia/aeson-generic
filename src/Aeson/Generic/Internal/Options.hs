{-# LANGUAGE
    DataKinds,
    FlexibleInstances,
    KindSignatures,
    PolyKinds,
    TypeOperators #-}

module Aeson.Generic.Internal.Options where

import Data.Kind (Type)
import GHC.Generics
import GHC.TypeLits (Symbol)

-- | Sort of extensible data types.
--
-- This also has the nice property that \"constructors\" are declared in the
-- type namespace, so they don't need to be promoted (i.e., prefixed with a
-- quote).
-- For that reason, as well as for the sake of uniformity, we encode all our
-- data types in this way, even if they are not meant to be extended.
type Data a = a -> Type

-- | Sort of nominalized data types.
--
-- This is mechanically equivalent to 'Data', but for the sake of
-- documentation, it distinguishes data types with only one constructor
-- of the same name, i.e., newtypes, whose purpose is to distinguish
-- the meaning of types whose representation is otherwise the same.
--
-- This palliates the lack of record syntax at the level of types.
--
-- === _Example_
--
-- Here's an example of a record with two fields of the same type,
-- distinguished by the field names:
--
-- > data Options = Options
-- >   { fieldModifier :: String -> String
-- >   , constructorModifier :: String -> String
-- >   , sumEncoding :: SumEncoding
-- >   }
--
-- Alternatively, newtypes allow us to make that distinction at the level of
-- types instead:
--
-- > data Options = Options
-- >   FieldModifier
-- >   ConstructorModifier
-- >   SumEncoding
-- >
-- > newtype FieldModifier = FieldModifier (String -> String)
-- > newtype ConstructorModifier = ConstructorModifier (String -> String)
--
-- The definition of 'Options' is a promoted reproduction of that example.
type Newtype a = a -> Type

-- | Type of options.
--
-- @
-- data Options = 'Options'
--   FieldModifier
--   ConstructorModifier
--   SumEncoding
-- @
data OPTIONS
data Options ::
  Newtype FIELDMODIFIER ->
  Newtype CONSTRUCTORMODIFIER ->
  Data ENCODING ->
  Data OPTIONS

-- | Default options.
type DefaultOptions
  = Options
      (FieldModifier IdModifier)
      (ConstructorModifier IdModifier)
      (SumEncodingOr
        '[ AllNullary :?= Untagged
         , SingleConstructor :?= Untagged
         , AnyData :?= DefaultTagged
         ])

-- | Newtype of functions to modify field names.
--
-- @
-- newtype FieldModifier = 'FieldModifier' NameModifier
-- @
--
-- Although this uses the same underlying representation as 'CONSTRUCTORMODIFIER',
-- i.e., 'NAMEMODIFIER', it uses a different interpretation function,
-- which makes available the names of both the type and the constructor a field
-- belongs to.
data FIELDMODIFIER
data FieldModifier ::
  Data NAMEMODIFIER ->
  Newtype FIELDMODIFIER

-- | Newtype of functions to modify constructor names.
data CONSTRUCTORMODIFIER
data ConstructorModifier ::
  Data NAMEMODIFIER ->
  Newtype CONSTRUCTORMODIFIER

-- | Extensible type of functions to modify (field or constructor) names.
data NAMEMODIFIER
data IdModifier :: Data NAMEMODIFIER

-- | Extensible type of JSON encoding strategies.
data ENCODING

-- | Union of encoding strategies, indexed by a predicate on the generic types
-- they apply to.
--
-- To derive aeson instances for a generic type, the strategy with the first
-- matching predicate will be used.
--
-- If no predicate matches, this is a compile-time error.
--
-- === _Example_
--
-- The following strategy uses the 'Untagged' encoding for types whose
-- constructors are all nullary, or with only one constructor.
-- Otherwise, it uses the 'DefaultTagged' encoding by default.
--
-- > SumEncodingOr
-- >   '[ AllNullary :?= Untagged
-- >    , SingleConstructor :?= Untagged
-- >    , AnyData :?= DefaultTagged
-- >    ])
data SumEncodingOr (l :: [Data ENCODINGCHOICE]) :: Data ENCODING

-- | Type of one component of a union in 'SumEncodingOr'.
--
-- @p :?= e@ is a pair of a predicate @p@ and an encoding @e@.
data ENCODINGCHOICE
data (:?=) ::
  Data DATAPREDICATE ->
  Data ENCODING ->
  Data ENCODINGCHOICE

-- | Extensible type of predicates on generic data representations
-- (i.e., @Repr a@).
data DATAPREDICATE
data SingleConstructor :: Data DATAPREDICATE
data AllNullary        :: Data DATAPREDICATE
data AnyData           :: Data DATAPREDICATE

-- | Encoding where the constructor name is in a \"tag\" field,
-- and its contents are either in a \"contents\" field,
-- or unpacked in the same record.
data Tagged ::
  Newtype TAGFIELDNAME ->
  Newtype CONTENTSFIELDNAME ->
  Data NESTRECORDS ->
  Data ENCODING

-- | Newtype of names for the field containing the tag name
-- for the 'Tagged' encoding.
data TAGFIELDNAME
data TagFieldName :: Symbol -> Newtype TAGFIELDNAME

-- | Newtype of names for the field containing the contents name
-- for the 'Tagged' encoding.
data CONTENTSFIELDNAME
data ContentsFieldName :: Symbol -> Newtype CONTENTSFIELDNAME

-- | Type of options to nest record constructors (i.e., using curly braces in
-- their @data@ definition) in a contents field ('YesNestRecords'), or to unpack
-- them in the same object with the tag field ('NoNestRecords'), for the 'Tagged'
-- encoding.
data NESTRECORDS
data YesNestRecords :: Data NESTRECORDS
data NoNestRecords  :: Data NESTRECORDS

-- | 'Tagged' encoding with default options.
type DefaultTagged
  = Tagged
      (TagFieldName "tag")
      (ContentsFieldName "contents")
      NoNestRecords

-- | Encoding with no explicit constructor tag. Constructors are just unpacked.
data Untagged :: Data ENCODING

data a :+ b = a :+ b
