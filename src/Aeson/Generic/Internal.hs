{-# LANGUAGE PolyKinds #-}

module Aeson.Generic.Internal where

import Data.Aeson ()

newtype Generically (opts :: k) a = Generically a

